package com.droiddevgeeks.ixigo.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Vampire on 2017-08-30.
 */

public class ConnectionDetector
{

    /**
     * This methed is used to check whether internet is connected or not.
     * @param context
     * @return
     */
    public static boolean isConnectedToInternet(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
