package com.droiddevgeeks.ixigo.utils;

/**
 * Created by Vampire on 2017-08-30.
 */

public class AppConstant
{

    public static final long SPLASH_DURATION = 3000;
    public static final String MAIN_LANDING_FRAGMENT_TAG = "MainLandingFragment";
    public static final String FRAGMENT_TAG = "fragment";
    public static final String FRAGMENT_FLIGHT_SEARCH = "FlightSearchFragment";
    public static final String FRAGMENT_FLIGHT_SEARCH_RESULT = "FlightSearchResultFragment";
    public static final String FLIGHT_SELECTED_FRAGMENT = "SelectedFlightFragment";

    public static final String FLIGHT_RESULT_KEY = "FlightResultKey";
    public static final String FLIGHT_SELECTED_KEY = "FlightedSelectedKey";

    public static final int NO_RESULT = 400;
}
