package com.droiddevgeeks.ixigo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.droiddevgeeks.ixigo.base.BaseActivity;
import com.droiddevgeeks.ixigo.flight.FlightSearchFragment;
import com.droiddevgeeks.ixigo.flight.FlightSearchResultFragment;
import com.droiddevgeeks.ixigo.flight.FlightSearchVO;
import com.droiddevgeeks.ixigo.flight.SelectedFlightFragment;
import com.droiddevgeeks.ixigo.fragment.MainLandingFragment;
import com.droiddevgeeks.ixigo.interfaces.IFragmentChange;
import com.droiddevgeeks.ixigo.utils.AppConstant;

import java.util.ArrayList;

public class MainLandingActivity extends BaseActivity implements IFragmentChange
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_landing);

        if (savedInstanceState == null)
        {
            addFragment(R.id.container, new MainLandingFragment(),false);
        }
        else
        {
            Fragment fragment = getSupportFragmentManager().getFragment(savedInstanceState, "SavedFragment");
            replaceFragment(R.id.container, fragment, false);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        getSupportFragmentManager().putFragment(outState, "SavedFragment", getSupportFragmentManager().findFragmentById(R.id.container));
        super.onSaveInstanceState(outState);
    }


    /**
     * This is interface method ,responsible for adding or replacing fragment over activity.
     * @param bundle This param is used to pass data from 1 fragment to another
     */
    @Override
    public void onFragmentChange(Bundle bundle)
    {
        String tag  = bundle.getString(AppConstant.FRAGMENT_TAG);
        switch (tag)
        {
            case AppConstant.FRAGMENT_FLIGHT_SEARCH:
                replaceFragment(R.id.container , new FlightSearchFragment() ,true);
                break;
            case AppConstant.FRAGMENT_FLIGHT_SEARCH_RESULT :
                Fragment fragment = new FlightSearchResultFragment();
                fragment.setArguments(bundle);
                replaceFragment(R.id.container ,fragment,true);
                break;
            case AppConstant.FLIGHT_SELECTED_FRAGMENT:
                Fragment flightSelectedFragment = new SelectedFlightFragment();
                flightSelectedFragment.setArguments(bundle);
                replaceFragment(R.id.container , flightSelectedFragment ,true);
                break;
        }


    }
}
