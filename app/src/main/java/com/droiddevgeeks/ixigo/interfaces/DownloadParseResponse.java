package com.droiddevgeeks.ixigo.interfaces;

import org.json.JSONObject;

/**
 * Created by Vampire on 2017-08-30.
 */

public abstract class DownloadParseResponse
{
    public abstract boolean parseJson(JSONObject jsonObject);
}
