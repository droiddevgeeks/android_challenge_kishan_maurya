package com.droiddevgeeks.ixigo.interfaces;

/**
 * Created by Vampire on 2017-08-30.
 */

public interface IDownloadListener
{
    void onDownloadSuccess(DownloadParseResponse downloadParseResponse);
    void onDownloadFailed(int errorCode, String message);
}
