package com.droiddevgeeks.ixigo.interfaces;

import android.os.Bundle;

/**
 * Created by Vampire on 2017-08-30.
 */

public interface IFragmentChange
{

    void onFragmentChange(Bundle bundle);
}
