package com.droiddevgeeks.ixigo.flight;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.droiddevgeeks.ixigo.MainLandingActivity;
import com.droiddevgeeks.ixigo.R;
import com.droiddevgeeks.ixigo.utils.AppConstant;
import com.droiddevgeeks.ixigo.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Vampire on 2017-08-30.
 */

public class FlightSearchResultFragment extends Fragment implements View.OnClickListener
{

    private TextView cheapF, earlyF, bestF;
    private RecyclerView recyclerView;
    private ArrayList<FlightSearchVO> flightSearchVOs;
    private FlightSearchAdapter adapter;
    private boolean isSortPriceClick;
    private boolean isSortEarlyClick;
    private boolean isSortDepartClick;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            flightSearchVOs = getArguments().getParcelableArrayList(AppConstant.FLIGHT_RESULT_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.flight_search_result, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        ((MainLandingActivity) getActivity()).setToolbar(getResources().getString(R.string.flight_toolbar));

        cheapF = (TextView) view.findViewById(R.id.txtCheap);
        earlyF = (TextView) view.findViewById(R.id.txtEarly);
        bestF = (TextView) view.findViewById(R.id.txtBest);

        cheapF.setOnClickListener(this);
        earlyF.setOnClickListener(this);
        bestF.setOnClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.searchRecyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        recyclerView.setLayoutManager(layoutManager);
        adapter = new FlightSearchAdapter(flightSearchVOs);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (adapter != null)
        {
            adapter.registerListener(getContext());
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (adapter != null)
        {
            adapter.removeListener();
        }
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.txtCheap:
                cheapF.setTextColor(getResources().getColor(R.color.orange));
                earlyF.setTextColor(getResources().getColor(R.color.black));
                bestF.setTextColor(getResources().getColor(R.color.black));
                if (!isSortPriceClick)
                {
                    sortListByPriceInc();
                }
                else
                {
                    sortListByPriceDesc();
                }
                break;
            case R.id.txtEarly:
                cheapF.setTextColor(getResources().getColor(R.color.black));
                earlyF.setTextColor(getResources().getColor(R.color.orange));
                bestF.setTextColor(getResources().getColor(R.color.black));
                if (!isSortEarlyClick)
                {
                    sortListByEarlyTimeInc();
                }
                else
                {
                    sortListByEarlyTimeDesc();
                }
                break;
            case R.id.txtBest:
                cheapF.setTextColor(getResources().getColor(R.color.black));
                earlyF.setTextColor(getResources().getColor(R.color.black));
                bestF.setTextColor(getResources().getColor(R.color.orange));
                if (!isSortDepartClick)
                {
                    sortListByDepartTimeInc();
                }
                else
                {
                    sortListByDepartTimeDesc();
                }
                break;
        }
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (adapter != null)
        {
            adapter.removeListener();
            adapter = null;
            recyclerView = null;
        }
    }

    // increasing order of price
    private void sortListByPriceInc()
    {
        Collections.sort(flightSearchVOs, new Comparator<FlightSearchVO>()
        {
            @Override
            public int compare(FlightSearchVO lhs, FlightSearchVO rhs)
            {

                isSortPriceClick = true;
                if (lhs.getMinFare() > rhs.getMinFare())
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        });

        adapter.setDataList(flightSearchVOs);
    }

    // decreasing order of price
    private void sortListByPriceDesc()
    {
        Collections.sort(flightSearchVOs, new Comparator<FlightSearchVO>()
        {
            @Override
            public int compare(FlightSearchVO lhs, FlightSearchVO rhs)
            {

                isSortPriceClick = false;
                if (lhs.getMinFare() < rhs.getMinFare())
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        });

        adapter.setDataList(flightSearchVOs);
    }

    // increasing order of early timing
    private void sortListByEarlyTimeInc()
    {
        Collections.sort(flightSearchVOs, new Comparator<FlightSearchVO>()
        {
            @Override
            public int compare(FlightSearchVO lhs, FlightSearchVO rhs)
            {

                isSortEarlyClick = true;
                if (Long.parseLong(lhs.getArrivalTime()) > Long.parseLong(rhs.getArrivalTime()))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        });

        adapter.setDataList(flightSearchVOs);
    }

    // decreasing order of early timing
    private void sortListByEarlyTimeDesc()
    {
        Collections.sort(flightSearchVOs, new Comparator<FlightSearchVO>()
        {
            @Override
            public int compare(FlightSearchVO lhs, FlightSearchVO rhs)
            {

                isSortEarlyClick = false;
                if (Long.parseLong(lhs.getArrivalTime()) < Long.parseLong(rhs.getArrivalTime()))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        });

        adapter.setDataList(flightSearchVOs);
    }

    // increasing order of depart timing
    private void sortListByDepartTimeInc()
    {
        Collections.sort(flightSearchVOs, new Comparator<FlightSearchVO>()
        {
            @Override
            public int compare(FlightSearchVO lhs, FlightSearchVO rhs)
            {

                isSortDepartClick = true;
                if (Long.parseLong(lhs.getDepartureTime()) > Long.parseLong(rhs.getDepartureTime()))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        });

        adapter.setDataList(flightSearchVOs);
    }

    // decreasing order of depart timing
    private void sortListByDepartTimeDesc()
    {
        Collections.sort(flightSearchVOs, new Comparator<FlightSearchVO>()
        {
            @Override
            public int compare(FlightSearchVO lhs, FlightSearchVO rhs)
            {

                isSortDepartClick = false;
                if (Long.parseLong(lhs.getDepartureTime()) < Long.parseLong(rhs.getDepartureTime()))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        });

        adapter.setDataList(flightSearchVOs);
    }
}
