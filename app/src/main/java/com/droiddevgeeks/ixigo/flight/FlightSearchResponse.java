package com.droiddevgeeks.ixigo.flight;

import android.content.Intent;

import com.droiddevgeeks.ixigo.interfaces.DownloadParseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Vampire on 2017-08-30.
 */

public class FlightSearchResponse extends DownloadParseResponse
{

    private ArrayList<FlightSearchVO> flightSearchVOs;

    @Override
    public boolean parseJson(JSONObject jsonObject)
    {
        JSONArray jsonArray = null;

        try
        {
            jsonArray = jsonObject.getJSONArray("flights");
            int len = jsonArray.length();
            if (len == 0)
            {
                return false;
            }

            JSONObject appendixObject = jsonObject.getJSONObject("appendix");
            JSONObject airLinesCode = appendixObject.getJSONObject("airlines");
            HashMap<String ,String> airLineCodeMap = new HashMap<>();
            airLineCodeMap.put("SG" ,airLinesCode.optString("SG"));
            airLineCodeMap.put("AI" ,airLinesCode.optString("AI"));
            airLineCodeMap.put("G8" ,airLinesCode.optString("G8"));
            airLineCodeMap.put("9W" ,airLinesCode.optString("9W"));
            airLineCodeMap.put("6E" ,airLinesCode.optString("6E"));

            JSONObject providerJson = appendixObject.getJSONObject("providers");
            HashMap<Integer,String> providerMap = new HashMap<>();
            providerMap.put(1 ,providerJson.optString("1"));
            providerMap.put(2 ,providerJson.optString("2"));
            providerMap.put(3 ,providerJson.optString("3"));
            providerMap.put(4 ,providerJson.optString("4"));

            flightSearchVOs = new ArrayList<>();
            for (int i = 0; i < len; i++)
            {
                JSONObject flightSearchJson = jsonArray.getJSONObject(i);
                FlightSearchVO flightVo = new FlightSearchVO();
                flightVo.setOriginCode(flightSearchJson.optString("originCode"));
                flightVo.setDestinationCode(flightSearchJson.optString("destinationCode"));
                flightVo.setDepartureTime(flightSearchJson.optString("departureTime"));
                flightVo.setArrivalTime(flightSearchJson.optString("arrivalTime"));
                flightVo.setAirlineCode(flightSearchJson.optString("airlineCode"));
                flightVo.setTravelClass(flightSearchJson.optString("class"));

                JSONArray fareJsonArray = flightSearchJson.getJSONArray("fares");
                ArrayList<FaresVO> faresVOList = new ArrayList<>();
                for (int j = 0; j < fareJsonArray.length(); j++)
                {
                    JSONObject fareObject = fareJsonArray.getJSONObject(j);
                    FaresVO faresVO = new FaresVO();
                    faresVO.setProviderId(fareObject.optInt("providerId"));
                    faresVO.setFare(fareObject.optInt("fare"));
                    faresVO.setProviderMap(providerMap);
                    faresVOList.add(faresVO);
                }

                Collections.sort(faresVOList, new Comparator<FaresVO>()
                {
                    @Override
                    public int compare(FaresVO faresVO, FaresVO t1)
                    {
                        return faresVO.getFare() - t1.getFare();
                    }
                });

                flightVo.setFares(faresVOList);
                flightVo.setMinFare(faresVOList.get(0).getFare());
                flightVo.setAirLineCodeMap(airLineCodeMap);
                flightSearchVOs.add(flightVo);
            }
            return true;
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList<FlightSearchVO> getFlightSearchVOs()
    {
        return flightSearchVOs;
    }
}
