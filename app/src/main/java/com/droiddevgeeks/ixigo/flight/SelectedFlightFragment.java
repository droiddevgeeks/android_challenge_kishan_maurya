package com.droiddevgeeks.ixigo.flight;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.droiddevgeeks.ixigo.MainLandingActivity;
import com.droiddevgeeks.ixigo.R;
import com.droiddevgeeks.ixigo.utils.AppConstant;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vampire on 2017-08-30.
 */

public class SelectedFlightFragment extends Fragment
{

    private FlightSearchVO flightSearchVO;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            flightSearchVO = getArguments().getParcelable(AppConstant.FLIGHT_SELECTED_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.selected_flight_layout, container ,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        ((MainLandingActivity) getActivity()).setToolbar(flightSearchVO.getOriginCode() + " - " + flightSearchVO.getDestinationCode());

        ListView providerListView = (ListView)view.findViewById(R.id.bookingOptionList);
        SelectedFlightAdapter adapter = new SelectedFlightAdapter(getContext() , flightSearchVO.getFares());
        providerListView.setAdapter(adapter);

        TextView txtAirlineName = (TextView)view.findViewById(R.id.txtAirlineName);
        String airlineCode = flightSearchVO.getAirlineCode();
        if(flightSearchVO.getAirLineCodeMap().containsKey(airlineCode))
        {
            String airLineName = flightSearchVO.getAirLineCodeMap().get(airlineCode);
            txtAirlineName.setText(airLineName);
        }
        else
        {
            txtAirlineName.setText(airlineCode);
        }

        TextView txtToFromCity = (TextView)view.findViewById(R.id.txtToFromCity);
        txtToFromCity.setText(flightSearchVO.getOriginCode() + " - " + flightSearchVO.getDestinationCode());

        TextView txtTravelClass = (TextView)view.findViewById(R.id.txtTravelClass);
        txtTravelClass.setText(flightSearchVO.getAirlineCode() + " ---- " + flightSearchVO.getTravelClass());

        long t1 = Long.parseLong(flightSearchVO.getArrivalTime());
        long t2 = Long.parseLong(flightSearchVO.getDepartureTime());
        long t3 = t2-t1;

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        Date arrivalDate = new Date(t1);
        Date departDate = new Date(t2);
        Date duration = new Date(t3);

        TextView txtFromTime = (TextView)view.findViewById(R.id.txtFromTime);
        txtFromTime.setText(flightSearchVO.getOriginCode() + " " + formatter.format(arrivalDate));

        TextView txtToTime = (TextView)view.findViewById(R.id.txtToTime);
        txtToTime.setText(flightSearchVO.getDestinationCode()+" "  + formatter.format(departDate) );

        TextView txtTimeDuration = (TextView)view.findViewById(R.id.txtTimeDuration);
        txtTimeDuration.setText(formatter.format(duration));



    }
}
