package com.droiddevgeeks.ixigo.flight;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Vampire on 2017-08-30.
 */

public class FlightSearchVO implements Parcelable
{
    private String originCode;
    private String destinationCode;
    private String departureTime;
    private String arrivalTime;
    private ArrayList<FaresVO> fares;
    private int minFare;
    private String airlineCode;
    private String travelClass;
    private HashMap<String ,String> airLineCodeMap;

    public FlightSearchVO(){}
    public FlightSearchVO(Parcel parcel)
    {
        this();
        this.originCode = parcel.readString();
        this.destinationCode = parcel.readString();
        this.departureTime = parcel.readString();
        this.arrivalTime = parcel.readString();
        this.minFare = parcel.readInt();
        parcel.readTypedList(fares , FaresVO.CREATOR);
        this.airlineCode = parcel.readString();
        this.travelClass = parcel.readString();
        parcel.readMap(airLineCodeMap , String.class.getClassLoader());


    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public FlightSearchVO createFromParcel(Parcel in)
        {
            return new FlightSearchVO(in);
        }

        public FlightSearchVO[] newArray(int size)
        {
            return new FlightSearchVO[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(this.originCode);
        parcel.writeString(this.destinationCode);
        parcel.writeString(this.departureTime);
        parcel.writeString(this.arrivalTime);
        parcel.writeInt(this.minFare);
        parcel.writeList(this.fares);
        parcel.writeString(this.airlineCode);
        parcel.writeString(this.travelClass);
        parcel.writeMap(airLineCodeMap);

    }

    public String getOriginCode()
    {
        return originCode;
    }

    public void setOriginCode(String originCode)
    {
        this.originCode = originCode;
    }

    public String getDestinationCode()
    {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode)
    {
        this.destinationCode = destinationCode;
    }

    public String getDepartureTime()
    {
        return departureTime;
    }

    public void setDepartureTime(String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getArrivalTime()
    {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public int getMinFare()
    {
        return minFare;
    }

    public void setMinFare(int minFare)
    {
        this.minFare = minFare;
    }

    public ArrayList<FaresVO> getFares()
    {
        return fares;
    }

    public void setFares(ArrayList<FaresVO> fares)
    {
        this.fares = fares;
    }

    public String getAirlineCode()
    {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode)
    {
        this.airlineCode = airlineCode;
    }

    public String getTravelClass()
    {
        return travelClass;
    }

    public void setTravelClass(String travelClass)
    {
        this.travelClass = travelClass;
    }


    public HashMap<String, String> getAirLineCodeMap()
    {
        return airLineCodeMap;
    }

    public void setAirLineCodeMap(HashMap<String, String> airLineCodeMap)
    {
        this.airLineCodeMap = airLineCodeMap;
    }

}
