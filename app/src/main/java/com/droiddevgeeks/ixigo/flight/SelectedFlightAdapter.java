package com.droiddevgeeks.ixigo.flight;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.droiddevgeeks.ixigo.R;

import java.util.ArrayList;

/**
 * Created by Vampire on 2017-08-30.
 */

public class SelectedFlightAdapter extends BaseAdapter
{

    private ArrayList<FaresVO> faresVOs;
    private LayoutInflater inflater = null;

    public SelectedFlightAdapter(Context context, ArrayList<FaresVO> faresVO)
    {
        this.faresVOs = faresVO;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return faresVOs.size();
    }

    public Object getItem(int position)
    {
        return position;
    }

    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        ViewHolder holder;
        if (view == null)
        {
            view = inflater.inflate(R.layout.booking_option_row, null);
            holder = new ViewHolder();
            holder.price = (TextView) view.findViewById(R.id.txtFare);
            holder.bookNow = (TextView) view.findViewById(R.id.txtBookNow);
            holder.provider = (TextView) view.findViewById(R.id.txtProvider);
            view.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) view.getTag();
        }

        /**
         * At time of json parsing we have created hashmap for airline code and providerInfo.
         * Using this hashmap , we will map 1 to MMT,2 to YATRA or GOIBIBO etc
         */
        int providerCode = faresVOs.get(i).getProviderId();
        if (faresVOs.get(i).getProviderMap().containsKey(providerCode))
        {
            String providerName = faresVOs.get(i).getProviderMap().get(providerCode);
            holder.provider.setText(providerName);
        }
        else
        {
            holder.provider.setText("" + providerCode);
        }

        String fare = view.getContext().getResources().getString(R.string.rupee_symbol) + faresVOs.get(i).getFare();
        holder.price.setText(fare);
        return view;
    }

    private static class ViewHolder
    {
        private TextView price;
        private TextView bookNow;
        private TextView provider;
    }
}
