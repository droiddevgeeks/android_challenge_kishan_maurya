package com.droiddevgeeks.ixigo.flight;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.droiddevgeeks.ixigo.MainLandingActivity;
import com.droiddevgeeks.ixigo.R;
import com.droiddevgeeks.ixigo.interfaces.IFragmentChange;
import com.droiddevgeeks.ixigo.utils.AppConstant;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Vampire on 2017-08-30.
 */

public class FlightSearchAdapter extends RecyclerView.Adapter<FlightSearchViewHolder>
{

    private IFragmentChange listener;
    private List<FlightSearchVO> flightSearchVOs;

    public FlightSearchAdapter(List<FlightSearchVO> flightSearchVOs)
    {
        this.flightSearchVOs = flightSearchVOs;
    }

    /**
     * This method is used to add listener in adapter.
     * @param context
     */
    public void registerListener(Context context)
    {
        listener = (MainLandingActivity) context;
    }

    /**
     * This is used to remove listneer from adapter when no use
     */
    public void removeListener()
    {
        listener = null;
    }

    @Override
    public FlightSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_search_row, parent, false);
        return new FlightSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FlightSearchViewHolder holder, final int position)
    {
        /**
         * At time of json parsing we have created hashmap for airline code and providerInfo.
         * Using this hashmap , we will map SG to Spice Jet ,AI to Air India etc
         */
        String airlineCode = flightSearchVOs.get(position).getAirlineCode();
        if(flightSearchVOs.get(position).getAirLineCodeMap().containsKey(airlineCode))
        {
            String airLineName = flightSearchVOs.get(position).getAirLineCodeMap().get(airlineCode);
            holder.airLineName.setText(airLineName);
        }
        else
        {
            holder.airLineName.setText(airlineCode);
        }

        long t1 = Long.parseLong(flightSearchVOs.get(position).getArrivalTime());
        long t2 = Long.parseLong(flightSearchVOs.get(position).getDepartureTime());

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        Date arrivalDate = new Date(t1);
        Date departDate = new Date(t2);

        holder.arrivalTime.setText(formatter.format(arrivalDate));
        holder.departTime.setText(formatter.format(departDate));

        String price = holder.itemView.getContext().getResources().getString(R.string.rupee_symbol) + flightSearchVOs.get(position).getMinFare();
        holder.price.setText(price);

        /*onclick of flight search row , we will open new fragment ,
        selectflight fragment to display other details of flight and to proceed for booking*/
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstant.FRAGMENT_TAG , AppConstant.FLIGHT_SELECTED_FRAGMENT);
                bundle.putParcelable(AppConstant.FLIGHT_SELECTED_KEY ,flightSearchVOs.get(position));
                listener.onFragmentChange(bundle);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return flightSearchVOs.size();
    }


    /**
     * this method is used to set or notify data changed in adapter.
     * This is basically used in Sorting using Price , Time etc
     * @param flightSearchVOs
     */
    public void setDataList(List<FlightSearchVO> flightSearchVOs)
    {
        this.flightSearchVOs = flightSearchVOs;
        notifyDataSetChanged();
    }
}
