package com.droiddevgeeks.ixigo.flight;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.droiddevgeeks.ixigo.R;

/**
 * Created by Vampire on 2017-08-30.
 */

public class FlightSearchViewHolder extends RecyclerView.ViewHolder
{
    public TextView airLineName;
    public TextView departTime;
    public TextView arrivalTime;
    public TextView price;


    public FlightSearchViewHolder(View itemView)
    {
        super(itemView);
        airLineName = (TextView) itemView.findViewById(R.id.txtAirlineName);
        departTime = (TextView) itemView.findViewById(R.id.txtDepartTime);
        arrivalTime = (TextView) itemView.findViewById(R.id.txtArrivalTime);
        price = (TextView) itemView.findViewById(R.id.txtPrice);
    }


}
