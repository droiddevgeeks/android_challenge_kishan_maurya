package com.droiddevgeeks.ixigo.flight;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by Vampire on 2017-08-30.
 */

public class FaresVO implements Parcelable
{

    private int providerId;
    private int fare;
    private HashMap<Integer ,String> providerMap;


    public FaresVO(){}
    public FaresVO(Parcel parcel)
    {
        this.providerId = parcel.readInt();
        this.fare = parcel.readInt();
        parcel.readMap(providerMap , String.class.getClassLoader());

    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
    {
        public FaresVO createFromParcel(Parcel in)
        {
            return new FaresVO(in);
        }

        public FaresVO[] newArray(int size)
        {
            return new FaresVO[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeInt(this.providerId);
        parcel.writeInt(this.fare);
        parcel.writeMap(providerMap);

    }

    public int getProviderId()
    {
        return providerId;
    }

    public void setProviderId(int providerId)
    {
        this.providerId = providerId;
    }

    public int getFare()
    {
        return fare;
    }

    public void setFare(int fare)
    {
        this.fare = fare;
    }

    public HashMap<Integer, String> getProviderMap()
    {
        return providerMap;
    }

    public void setProviderMap(HashMap<Integer, String> providerMap)
    {
        this.providerMap = providerMap;
    }
}
