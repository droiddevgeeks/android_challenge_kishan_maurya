package com.droiddevgeeks.ixigo.flight;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.droiddevgeeks.ixigo.MainLandingActivity;
import com.droiddevgeeks.ixigo.R;
import com.droiddevgeeks.ixigo.download.FetchDataAsync;
import com.droiddevgeeks.ixigo.interfaces.DownloadParseResponse;
import com.droiddevgeeks.ixigo.interfaces.IDownloadListener;
import com.droiddevgeeks.ixigo.interfaces.IFragmentChange;
import com.droiddevgeeks.ixigo.utils.APIUrls;
import com.droiddevgeeks.ixigo.utils.AppConstant;
import com.droiddevgeeks.ixigo.utils.ConnectionDetector;

/**
 * Created by Vampire on 2017-08-30.
 */

public class FlightSearchFragment extends Fragment implements View.OnClickListener, IDownloadListener
{

    private View viewOneWay, viewTwoWay;
    private TextView tocity, fromCity;

    private ProgressDialog dialog;
    private IFragmentChange listener;
    private FetchDataAsync fetchDataAsync;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.flight_search_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        ((MainLandingActivity) getActivity()).setToolbar(getResources().getString(R.string.flight_toolbar));

        TextView oneWay = (TextView) view.findViewById(R.id.txtOneWay);
        oneWay.setOnClickListener(this);
        TextView twoWay = (TextView) view.findViewById(R.id.txtTwoWay);
        twoWay.setOnClickListener(this);

        viewOneWay = (View) view.findViewById(R.id.viewOneWay);
        viewTwoWay = (View) view.findViewById(R.id.viewTwoWay);

        tocity = (TextView) view.findViewById(R.id.txtToCity);
        fromCity = (TextView) view.findViewById(R.id.txtFromCity);

        ImageView cityToggle = (ImageView) view.findViewById(R.id.imgCityToggle);
        cityToggle.setOnClickListener(this);

        TextView txtSearchButton = (TextView) view.findViewById(R.id.txtSearchButton);
        txtSearchButton.setOnClickListener(this);

    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        listener = (MainLandingActivity) context;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.txtOneWay:
                viewOneWay.setBackgroundColor(getResources().getColor(R.color.dark_gray));
                viewOneWay.setVisibility(View.VISIBLE);
                viewTwoWay.setVisibility(View.INVISIBLE);
                break;
            case R.id.txtTwoWay:
                viewTwoWay.setBackgroundColor(getResources().getColor(R.color.dark_gray));
                viewTwoWay.setVisibility(View.VISIBLE);
                viewOneWay.setVisibility(View.INVISIBLE);
                break;
            case R.id.imgCityToggle:
                toggleToFromCities();
                break;
            case R.id.txtSearchButton:
                if (ConnectionDetector.isConnectedToInternet(getContext()))
                {
                    displayProgressBar();
                    searchFlight();
                }
                else
                {
                    Toast.makeText(getContext(), getResources().getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    /**
     * This method is used to perform async related task. Fetching data from server.
     * Once data is downloaded from server , OnDownloadSuccess will get informed
     * if any error occurs then onDownloadFailed will get updated or notified
     */
    private void searchFlight()
    {
        fetchDataAsync = new FetchDataAsync(new FlightSearchResponse(), this);
        fetchDataAsync.execute(APIUrls.FLIGHT_SEARCH);

    }

    private void displayProgressBar()
    {
        dialog = new ProgressDialog(getContext());
        dialog.setMessage(getContext().getResources().getString(R.string.progress_message));
        dialog.show();
    }

    private void closeProgressBar()
    {
        if (dialog != null)
        {
            dialog.dismiss();
        }
    }

    /**
     * This method switches To and From city in Flight Search Layout.
     */
    private void toggleToFromCities()
    {
        String to = tocity.getText().toString();
        String from = fromCity.getText().toString();

        tocity.setText(from);
        fromCity.setText(to);
    }

    /**
     * When asynctask is completed then using listner ,
     * we update this method with desired data.
     * @param downloadParseResponse
     */
    @Override
    public void onDownloadSuccess(DownloadParseResponse downloadParseResponse)
    {
        closeProgressBar();
        if (downloadParseResponse instanceof FlightSearchResponse)
        {
            FlightSearchResponse response = (FlightSearchResponse) downloadParseResponse;
            Bundle bundle = new Bundle();
            bundle.putString(AppConstant.FRAGMENT_TAG, AppConstant.FRAGMENT_FLIGHT_SEARCH_RESULT);
            bundle.putParcelableArrayList(AppConstant.FLIGHT_RESULT_KEY , response.getFlightSearchVOs());
            listener.onFragmentChange(bundle);
        }

    }

    @Override
    public void onDownloadFailed(int errorCode, String message)
    {
        closeProgressBar();
        Toast.makeText(getContext(), getResources().getString(R.string.no_result), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        if (fetchDataAsync != null)
        {
            Log.v("Cancel", "onDestroy ");
            fetchDataAsync.cancel(true);
            fetchDataAsync = null;
        }
        dialog = null;
    }
}
