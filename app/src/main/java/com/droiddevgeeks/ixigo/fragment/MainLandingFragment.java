package com.droiddevgeeks.ixigo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.droiddevgeeks.ixigo.MainLandingActivity;
import com.droiddevgeeks.ixigo.R;
import com.droiddevgeeks.ixigo.interfaces.IFragmentChange;
import com.droiddevgeeks.ixigo.utils.AppConstant;

/**
 * Created by Vampire on 2017-08-30.
 */

public class MainLandingFragment extends Fragment implements View.OnClickListener
{

    private IFragmentChange fragmentListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.main_landing_fragment_layout ,container ,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        ((MainLandingActivity)getActivity()).setToolbar(getResources().getString(R.string.app_name));
        TextView flight = (TextView)view.findViewById(R.id.txtFlightSearch);
        TextView hotel = (TextView)view.findViewById(R.id.txtHotelSearch);
        TextView trips = (TextView)view.findViewById(R.id.txtTripSearch);

        flight.setOnClickListener(this);
        hotel.setOnClickListener(this);
        trips.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        fragmentListener = (IFragmentChange)context;
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.txtFlightSearch:
                if(fragmentListener !=null)
                {
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstant.FRAGMENT_TAG , AppConstant.FRAGMENT_FLIGHT_SEARCH);
                    fragmentListener.onFragmentChange(bundle);
                }
                break;
            case R.id.txtHotelSearch:
                Toast.makeText(getContext() , getResources().getString(R.string.coming_soon),Toast.LENGTH_SHORT).show();
                break;
            case R.id.txtTripSearch:
                Toast.makeText(getContext() , getResources().getString(R.string.coming_soon),Toast.LENGTH_SHORT).show();
                break;

        }
    }
}
