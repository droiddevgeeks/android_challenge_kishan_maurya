package com.droiddevgeeks.ixigo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.widget.VideoView;

import com.droiddevgeeks.ixigo.base.BaseActivity;

/**
 * Created by Vampire on 2017-08-30.
 */

public class SplashActivity extends BaseActivity
{

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);

        try
        {
            VideoView videoHolder = (VideoView)findViewById(R.id.splashVideo);
            Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash_video);
            videoHolder.setVideoURI(video);
            videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
            {

                public void onCompletion(MediaPlayer mp)
                {
                    startMainLandingActivity();
                }

            });
            videoHolder.start();
        }
        catch (Exception ex)
        {
            startMainLandingActivity();
        }
    }

    /**
     * This method is called when splash video gets over and is used to finish splash activity
     * and loading main landing activity
     */
    private void startMainLandingActivity()
    {
        Intent newActivity = new Intent(SplashActivity.this , MainLandingActivity.class);
        startActivity(newActivity);
        finish();
    }

}
