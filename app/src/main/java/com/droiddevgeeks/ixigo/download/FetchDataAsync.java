package com.droiddevgeeks.ixigo.download;

import android.os.AsyncTask;
import android.util.Log;

import com.droiddevgeeks.ixigo.interfaces.DownloadParseResponse;
import com.droiddevgeeks.ixigo.interfaces.IDownloadListener;
import com.droiddevgeeks.ixigo.utils.AppConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Vampire on 2017-08-30.
 */
/**
 * This class is used to fetch data from server.
 * I am using http url connection for data fetching over server
 */
public class FetchDataAsync extends AsyncTask<String, Integer, String>
{
    private DownloadParseResponse downloadParseResponse;
    private WeakReference<IDownloadListener> iDownloadListener;

    /**
     *
     * @param downloadParseResponse This param contain object of class which is extending this class.
     *                              Parent class can contain child class reference
     * @param listener
     */
    public FetchDataAsync(DownloadParseResponse downloadParseResponse, IDownloadListener listener)
    {
        this.downloadParseResponse = downloadParseResponse;
        iDownloadListener = new WeakReference<>(listener);
    }

    @Override
    protected String doInBackground(String... params)
    {
        URL url = null;
        try
        {
            url = new URL(params[0]);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String res = null;
            StringBuilder stringBuilder = new StringBuilder();
            while ((res = bufferedReader.readLine()) != null)
            {
                if(isCancelled())
                {
                    Log.v("Cancel", "cancel");
                    stringBuilder = null;
                    httpURLConnection.disconnect();
                    inputStream.close();
                    bufferedReader.close();
                    break;
                }
                stringBuilder = stringBuilder.append(res);
            }
            res = stringBuilder.toString();
            httpURLConnection.disconnect();
            inputStream.close();
            return res;
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s)
    {
        super.onPostExecute(s);
        if (s != null)
        {
            try
            {
                if (downloadParseResponse != null)
                {
                    /* parseJson of child class is called as we have pass child class object.
                     Once Json parsing gets completed ,it return true or false based on parsing result*/
                    if (downloadParseResponse.parseJson(new JSONObject(s)))
                    {
                        /*if parsing is successful then we notify downloadSuccess*/
                        iDownloadListener.get().onDownloadSuccess(downloadParseResponse);
                    }
                    else
                    {
                         /*if parsing is failed or any error occured then we notify downloadFailed*/
                        iDownloadListener.get().onDownloadFailed(AppConstant.NO_RESULT,"No Result found");
                    }
                }
                downloadParseResponse = null;
                iDownloadListener = null;
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
