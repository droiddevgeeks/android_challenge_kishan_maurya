package com.droiddevgeeks.ixigo.base;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.droiddevgeeks.ixigo.R;

/**
 * Created by Vampire on 2017-08-30.
 */

/**
 * This base activity is used to replace or add fragment.
 * All common fragment related task will be done here
 */
public class BaseActivity extends AppCompatActivity
{

    /**
     * This method is used to add fragment on given activity container
     * @param containerId
     * @param fragment
     * @param isAddToBackStack
     */
    public void addFragment(int containerId, Fragment fragment ,boolean isAddToBackStack)
    {
        if (isAddToBackStack)
        {
            getSupportFragmentManager().beginTransaction().add(containerId, fragment).addToBackStack(null).commit();
        }
        else
        {
            getSupportFragmentManager().beginTransaction().add(containerId, fragment).commit();
        }

    }

    /**
     * This method is used to replace fragment on given activity container
     * @param containerId
     * @param fragment
     * @param isAddToBackStack
     */
    public void replaceFragment(int containerId, Fragment fragment, boolean isAddToBackStack)
    {
        if (isAddToBackStack)
        {
            getSupportFragmentManager().beginTransaction().replace(containerId, fragment).addToBackStack(null).commit();
        }
        else
        {
            getSupportFragmentManager().beginTransaction().replace(containerId, fragment).commit();
        }
    }

    /**
     * this method is used to set toolbar Title on every page
     * @param title
     */
    public void setToolbar(String title)
    {
        TextView pageTitle = ((TextView)findViewById(R.id.pageTitle));
        pageTitle.setText(title);
    }

}
